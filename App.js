import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, ScrollView, View, Image, Button, Linking } from 'react-native';
import axios from 'axios'

export default function App() {
  const getSlightPayPayment = async (url) => {
    try {
      let result = await axios.get(url)

      if (result.data.includes('backendToken')) {
        return result.data.substr(result.data.indexOf('backendToken') + 16, result.data.indexOf('\", e.sandboxStatus') - result.data.indexOf('backendToken') - 16)
      } else {
        return { error: true, motivo: 'No se obtuvo correctamente el token' }
      }
    } catch (e) {
      if (e.message.includes('Request failed with status code 422')) {
        return { error: true, motivo: 'El id de comercio no corresponde a la llave de SlightPay' }
      } else {
        return { error: true, motivo: 'Ocurrio un error con la integracion de SlightPay' }
      }
    }
  }

  const getPurchaseToken = async (token, products, tax, shippingFee, mainUrl, discountCommerce) => {
    try {
      let result = await axios.post(`${mainUrl}/purchases/create-order`, {
        discountCommerce,
        products,
        tax,
        shippingFee,
        orderId: 1,
        urlPurchase: 'Sandbox Store App' // Nombre de la App
      }, {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      })

      return result.data.token
    } catch (e) {
      return { error: true, motivo: 'El monto total de una compra con Slightpay va desde los $300 hasta los $9000, si su compra supera este monto porfavor notifique al comercio para retirar la orden de compra.' }
    }
  }

  const createPurchaseSlightPay = async () => {
    // -----------------------------------------CONFIG SLIGHTPAY-----------------------------------------
    let idCommerce = '1'
    let keyCommerce = '744ad.59970dfb63d34764ac7c695c9f285ecf'
    let mainUrl = 'https://api-develop.slightpay.com'
    let url = `${mainUrl}/pay-button/${idCommerce}?sandbox=true&key=${keyCommerce}`

    let urlPayment = 'https://pagos-develop.slightpay.com'
    //---------------------------------------------------------------------------------------------------

    let tax = 50
    let shippingFee = 100
    let products = [
      { name: 'Jordan tenis Air Jordan 1 Mid', quantity: 1, amount: 4000 },
      { name: 'Sudadera Adidas L', quantity: 2, amount: 600 }
    ]
    let discountCommerce = 150

    let tokenCommerce = await getSlightPayPayment(url)
    let tokenPurchase

    if (!tokenCommerce.error) tokenPurchase = await getPurchaseToken(tokenCommerce, products, tax, shippingFee, mainUrl, discountCommerce)

    if (!tokenPurchase.error) {
      Linking.canOpenURL(`${urlPayment}/${tokenPurchase}`).then(supported => {
        if (supported) {
          Linking.openURL(`${urlPayment}/${tokenPurchase}`);
        } else {
          console.log("Don't know how to open URI: " + `${urlPayment}/${tokenPurchase}`);
        }
      });
    }
  }

  return (
    <ScrollView style={styles.container}>
      <Text style={{ fontSize: 20, paddingBottom: 20, paddingLeft: 20 }}>Sandbox Store App</Text>
      <StatusBar style="auto" />
      <Text style={{ fontSize: 16, paddingHorizontal: 20, textAlign: 'center', paddingBottom: 15, color: '#1A4CCD' }}>¡Tu orden ha sido generada con éxito!</Text>
      <View style={{ flexDirection: 'row', padding: 20 }}>
        <Image
          style={styles.tinyLogo}
          source={{
            uri: 'https://sandbox-store.slightpay.com/assets/jordan.jfif',
          }}
        />
        <View style={{ paddingLeft: 10 }}>
          <Text style={{ fontWeight: '700' }}>Artículo: </Text>
          <Text style={{ paddingBottom: 15 }}>Jordan tenis Air Jordan 1 Mid</Text>
          <Text style={{ fontWeight: '700' }}>Cantidad: </Text>
          <Text style={{ paddingBottom: 15 }}>1</Text>
          <Text style={{ fontWeight: '700' }}>Costo: </Text>
          <Text style={{ paddingBottom: 15 }}>$4000.00</Text>
        </View>
      </View>
      <View style={{ flexDirection: 'row', padding: 20 }}>
        <Image
          style={styles.tinyLogo}
          source={{
            uri: 'https://sandbox-store.slightpay.com/assets/adidas.png',
          }}
        />
        <View style={{ paddingLeft: 10 }}>
          <Text style={{ fontWeight: '700' }}>Artículo: </Text>
          <Text style={{ paddingBottom: 15 }}>Sudadera Adidas</Text>
          <Text style={{ fontWeight: '700' }}>Cantidad: </Text>
          <Text style={{ paddingBottom: 15 }}>2</Text>
          <Text style={{ fontWeight: '700' }}>Costo: </Text>
          <Text style={{ paddingBottom: 15 }}>$600.00</Text>
        </View>
      </View>
      <View style={{ margin: 20, backgroundColor: '#f7f7f7', padding: 20, marginBottom: 50 }}>
        <Text style={{ fontSize: 16, paddingBottom: 25 }}>Detalles de la orden</Text>
        <Text style={{ fontWeight: '700' }}>Número de orden: </Text>
        <Text style={{ paddingBottom: 15 }}>100</Text>
        <Text style={{ fontWeight: '700' }}>Subtotal: </Text>
        <Text style={{ paddingBottom: 15 }}>$5200.00</Text>
        <Text style={{ fontWeight: '700' }}>Envío: </Text>
        <Text style={{ paddingBottom: 15 }}>$100.00</Text>
        <Text style={{ fontWeight: '700' }}>Impuestos: </Text>
        <Text style={{ paddingBottom: 15 }}>$50.00</Text>
        <Text style={{ fontWeight: '700' }}>Total: </Text>
        <Text style={{ paddingBottom: 15 }}>$5350.00</Text>
        <View style={{ alignItems: 'center' }}>
          <Button title='Pagar con SlightPay' onPress={createPurchaseSlightPay}></Button>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 32,
  },
  tinyLogo: {
    width: 150,
    height: 150,
    borderWidth: 0.1,
    borderColor: 'black'
  },
});
