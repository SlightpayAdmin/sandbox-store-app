## Primeros Pasos
Para correr el proyecto de prueba se necesita:
- [Expo CLI](https://docs.expo.dev/)
- [Node JS](https://nodejs.org/es/download/)
- [Expo Go](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=es_MX&gl=US)

## Descargar dependencias
`npm install`

## Para correr el proyecto se utiliza el comando
`expo start`